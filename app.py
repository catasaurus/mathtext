from mathtext.api_gradio import build_html_block


if __name__ == "__main__":
    html_block = build_html_block()
    html_block.launch()
