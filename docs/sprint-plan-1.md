# sprint-plan-1
2/3/2023
* [] Convert fetching from gitlab to absolute paths
* [] Create a testset/dataset for number word tagging
* [] Read spacy docs on part of speech tagging
* [] Run example code for part of speach tagging found on spacy docs
* [] Find tutorial on spacy custom tagging
* [] Reproduce examples in tutorial
* [] Plan tasks to create custom tagger for mathtext tag_numbers

## Backlog
* [] Phoenetic number words
* [] Deep learning for number word translation
* [] Number word tagging/named entity extraction