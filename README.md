---
title: MathText
app_file: app.py
sdk: gradio
sdk_version: 3.15.0
license: agpl-3.0
---

## MathText NLU

Natural Language Understanding for math symbols, digits, and words with a Gradio user interface and REST API.

