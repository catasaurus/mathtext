""" Utilites for processing chatbot message text to extract number words (text2int)

  * [ ] has_thousands_separator
  * [ ] render_int(with_comma=False, exponential_notation=False)
  * [ ] render_num(with_comma=False, exponential_notation=False)
  * [ ] render_float(with_comma=False, exponential_notation=False)
  * [ ] num_to_str()
  * [ ] get_ints()
  * [ ] all functions need doctests and/or unittests

  * [ ] text2nums function that Seb (catasaurus) is working on
  * [ ] tag_nums function to return tokens tagged with num or POS (spacy does this)
  * [ ] tag_math_tokens returns POS including plus, minus, times, etc
  * [ ] extract_expression
  * [ ] >>> get_math_tokens("1+2") => ["1", "+", "2"]
  * [ ] >>> eval_math_tokens(["1", "+", "2"]) => 3
"""

import re
import spacy  # noqa
from transformers import pipeline
from collections import Counter

# import os
# os.environ['KMP_DUPLICATE_LIB_OK']='True'
# import spacy

# Change this according to what words should be corrected to
SPELL_CORRECT_MIN_CHAR_DIFF = 2

TOKENS2INT_ERROR_INT = 32202

ONES = [
    "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
    "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
    "sixteen", "seventeen", "eighteen", "nineteen",
]

CHAR_MAPPING = {
    "-": " ",
    "_": " ",
    "and": " ",
}
# CHAR_MAPPING.update((str(i), word) for i, word in enumerate([" " + s + " " for s in ONES]))

TOKEN_MAPPING = {
    "and": " ",
    "oh": "0",
}


# TODO: Maria and Ashyr see if you can implement this function
def has_thousands_separator(number_word):
    """ Return True if string contains a valid number representation with comma as thousands seperator

    # >>> has_thousands_separator('10,000')
    # True
    # >>> has_thousands_separator('9,123,456.')
    # True
    # >>> has_thousands_separator('123,456,789,012,345.678901')
    # True
    # >>> has_thousands_separator('123456789012345.678901')
    # False
    # >>> has_thousands_separator('123_456_789_012_345.678901')
    # False

    >>> has_thousands_separator('1,2,3')
    Traceback (most recent call last):
     ...
    ValueError: Invalid number representation. has_thousands_separator() only accepts a string representation of a single number (not a number sequence)
    """
    raise ValueError('Invalid number representation. has_thousands_separator() only accepts a string representation of a single number (not a number sequence)')


def is_int(x):
    """ Return False if values is not a valid integer and cannot be coerced into an integer

    >>> is_int('123')
    True
    >>> is_int('0.0')
    True
    >>> is_int(float('nan'))
    False
    >>> is_int('1.234e5')
    True
    >>> is_int(1234567)
    True
    """
    try:
        if float(x) == int(float(x)):
            return True
    except ValueError:
        pass
    return False


def render_int(x, with_comma=False, exponential_notation=False):
    """ Coerce integer into a string for inclusion in a chat text message. 

    >>> render_int(123)
    '123'
    >>> render_int(123_456)
    '123456'
    >>> render_int(123_456, with_comma=True)
    '123,456'
    """
    pass


def render_float(with_comma=False, exponential_notation=False):
    pass


def render_num(with_comma=False, exponential_notation=False):
    pass


def replace_words(s, word_dict, whole_word=True):
    """ Like str.replace(word_dict) - replace occurrences of a dict's keys with its values

    SEE ALSO: replace_tokens(text)

    >>> word_dict = {'Спорт': 'Досуг', 'russianA': 'englishA'}
    >>> replace_words('Спорт not russianA', word_dict)
    'Досуг not englishA'
    """
    word_sep = ''  # word separator regex pattern
    if whole_word:
        word_sep = r'\b'
    keys = (re.escape(k) for k in word_dict.keys())
    pattern = re.compile(f'{word_sep}(' + '|'.join(keys) + f'){word_sep}')
    return pattern.sub(lambda x: word_dict[x.group()], s)


def replace_substrings(s, word_dict, whole_word=False):
    """ Like replace_words except ignore word boundaries(default to whole_word=False)

    >>> word_dict = {'Спорт': 'Досуг', 'russianA': 'englishA'}
    >>> replace_substrings('Спорт notrussianA', word_dict)
    'Досуг notenglishA'
    """
    return replace_words(s, word_dict, whole_word=whole_word)


def find_char_diff(a, b):
    """ Character difference between two str - counts occurrences of chars independently. Not edit distance.

    >>> find_char_diff('eight', 'eht')
    2
    >>> find_char_diff('eight', 'iht')
    2
    >>> find_char_diff('eight', 'hit')
    2
    >>> find_char_diff('feet', 'feeet')
    1

    >>  edit_distance('eight', 'hit')
    3
    """

    char_counts_a = Counter(a)
    char_counts_b = Counter(b)
    char_diff = 0
    for i in char_counts_a:
        if i in char_counts_b:
            char_diff += abs(char_counts_a[i] - char_counts_b[i])
        else:
            char_diff += char_counts_a[i]
    return char_diff


def tokenize(text):
    text = text.lower()
    text = replace_tokens(
        ''.join(i for i in replace_chars(
            text, char_mapping=CHAR_MAPPING)
        ).split(),
        token_mapping=TOKEN_MAPPING)
    # print(text)
    text = [i for i in text if i != ' ']
    # print(text)
    output = []
    for word in text:
        # print(word)
        output.append(convert_word_to_int(word))
    output = [i for i in output if i != ' ']
    # print(output)
    return output


def detokenize(tokens):
    return ' '.join(tokens)


def replace_tokens(tokens, token_mapping=TOKEN_MAPPING):
    return [token_mapping.get(tok, tok) for tok in tokens]


def replace_chars(text, char_mapping=CHAR_MAPPING):
    return [char_mapping.get(c, c) for c in text]


def convert_word_to_int(in_word, numwords={}):
    # Converts a single word/str into a single int
    tens = ["", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]
    scales = ["hundred", "thousand", "million", "billion", "trillion"]
    if not numwords:
        for idx, word in enumerate(ONES):
            numwords[word] = idx
        for idx, word in enumerate(tens):
            numwords[word] = idx * 10
        for idx, word in enumerate(scales):
            numwords[word] = 10 ** (idx * 3 or 2)
    if in_word in numwords:
        # print(in_word)
        # print(numwords[in_word])
        return numwords[in_word]
    try:
        int(in_word)
        return int(in_word)
    except ValueError:
        pass
    # Spell correction using find_char_diff
    char_diffs = [find_char_diff(in_word, i) for i in ONES + tens + scales]
    min_char_diff = min(char_diffs)
    if min_char_diff <= SPELL_CORRECT_MIN_CHAR_DIFF:
        return char_diffs.index(min_char_diff)


def tokens2int(tokens):
    """ Takes a list of tokens and returns a int representation of them """
    types = []
    for i in tokens:
        if i <= 9:
            types.append(1)

        elif i <= 90:
            types.append(2)

        else:
            types.append(3)
    # print(tokens)
    if len(tokens) <= 3:
        current = 0
        for i, number in enumerate(tokens):
            if i != 0 and types[i] < types[i - 1] and current != tokens[i - 1] and types[i - 1] != 3:
                current += tokens[i] + tokens[i - 1]
            elif current <= tokens[i] and current != 0:
                current *= tokens[i]
            elif 3 not in types and 1 not in types:
                current = int(''.join(str(i) for i in tokens))
                break
            elif '111' in ''.join(str(i) for i in types) and 2 not in types and 3 not in types:
                current = int(''.join(str(i) for i in tokens))
                break
            else:
                current += number

    elif 3 not in types and 2 not in types:
        current = int(''.join(str(i) for i in tokens))

    else:
        count = 0
        current = 0
        for i, token in enumerate(tokens):
            count += 1
            if count == 2:
                if types[i - 1] == types[i]:
                    current += int(str(token) + str(tokens[i - 1]))
                elif types[i - 1] > types[i]:
                    current += tokens[i - 1] + token
                else:
                    current += tokens[i - 1] * token
                count = 0
            elif i == len(tokens) - 1:
                current += token

    return current


def text2int(text):
    # Wraps all of the functions up into one
    return tokens2int(tokenize(text))

###############################################
# Vish editdistance approach doesn't halt


def lev_dist(a, b):
    '''
    This function will calculate the levenshtein distance between two input
    strings a and b

    params:
        a (String) : The first string you want to compare
        b (String) : The second string you want to compare

    returns:
        This function will return the distance between string a and b.

    example:
        a = 'stamp'
        b = 'stomp'
        lev_dist(a,b)
        >> 1.0
    '''
    if not isinstance(a, str) and isinstance(b, str):
        raise ValueError(f"lev_dist() requires 2 strings not lev_dist({repr(a)}, {repr(b)}")
    if a == b:
        return 0

    def min_dist(s1, s2):

        print(f"{a[s1]}s1{b[s2]}s2 ", end='')
        if s1 >= len(a) or s2 >= len(b):
            return len(a) - s1 + len(b) - s2

        # no change required
        if a[s1] == b[s2]:
            return min_dist(s1 + 1, s2 + 1)

        return 1 + min(
            min_dist(s1, s2 + 1),      # insert character
            min_dist(s1 + 1, s2),      # delete character
            min_dist(s1 + 1, s2 + 1),  # replace character
        )

    dist = min_dist(0, 0)
    print(f"\n  lev_dist({a}, {b}) => {dist}")
    return dist


sentiment = pipeline(task="sentiment-analysis", model="distilbert-base-uncased-finetuned-sst-2-english")


def get_sentiment(text):
    return sentiment(text)
