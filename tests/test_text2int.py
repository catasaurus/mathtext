import unittest
from pathlib import Path

import pandas as pd

from mathtext.text2int import text2int

# The raw file URL has to be used for GitLab.
URL = "https://gitlab.com/tangibleai/community/mathtext/-/raw/main/mathtext/data/master_test_text2int.csv"

DATA_DIR = Path(__file__).parent.parent / "mathtext" / "data"
print(DATA_DIR)


class TestStringMethods(unittest.TestCase):

    def setUp(self):
        """Downloads a raw file from a givenURL and converts to DF"""
        self.df = pd.read_csv(URL)

    @staticmethod
    def get_text2int(text):
        """Makes a post request to the endpoint"""
        r = None
        try:
            r = text2int(text)
        except:
            pass
        return r

    def test_acc_score_text2int(self):
        """Calculates accuracy score for the function"""

        self.df["text2int"] = self.df["input"].apply(func=self.get_text2int)
        self.df["score"] = self.df[["output", "text2int"]].apply(
            lambda row: row[0] == row[1],
            axis=1
        )
        self.df.to_csv(DATA_DIR / "text2int_results.csv", index=False)
        acc_score = self.df["score"].mean().__round__(2)

        self.assertGreaterEqual(acc_score, 0.5, f"Accuracy score: '{acc_score}'. Value is too low!")


if __name__ == '__main__':
    unittest.main()
